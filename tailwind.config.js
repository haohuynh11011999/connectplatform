// eslint-disable-next-line @typescript-eslint/no-var-requires
const plugin = require("tailwindcss/plugin");

const SIDEBAR_WIDTH = 224;
const SIDEBAR_COMPACT_WIDTH = 56; // ! Can't be changed

const HEADER_HEIGHT = 56;

const toPx = (n) => `${n}px`;

module.exports = {
  // important: true,
  // purge: [],
  purge: [
    "./src/pages/**/*.{js,ts,jsx,tsx}",
    "./src/containers/**/*.{js,ts,jsx,tsx}",
    "./src/components/**/*.{js,ts,jsx,tsx}",
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    container: {
      // you can configure the container to be centered
      center: true,

      // or have default horizontal padding
      padding: "1rem",

      // default breakpoints but with 40px removed
      screens: {
        sm: "100%",
        md: "100%",
        lg: "1024px",
        xl: "1280px",
      },
    },
    fontFamily: {
      inter: ["Inter"],
    },
    extend: {
      colors: {
        primary: {
          25: "#fef7ee",
          40: "#fcf2e5",
          50: "#ffefdd",
          100: "#ffd3b2",
          200: "#fab784",
          300: "#f79c55",
          400: "#f37f26", // Primary
          500: "#d9660c",
          600: "#aa4f07",
          700: "#7a3704",
          800: "#4a2100",
          900: "#1e0800",
        },
        success: {
          50: "#dcfff5",
          100: "#b0ffe3",
          200: "#80ffd2",
          300: "#50ffc0",
          400: "#2affaf",
          500: "#1ae695", // Primary
          600: "#0bb374",
          700: "#008052",
          800: "#004d30",
          900: "#001b0e",
        },
        warning: {
          50: "#ffeddf",
          100: "#fcceb6",
          200: "#f5ae8a",
          300: "#ef8f5d",
          400: "#e96f2f", // Primary
          500: "#d05616",
          600: "#a24310",
          700: "#742f09",
          800: "#471b02",
          900: "#1e0700",
        },
        alert: {
          50: "#ffe4e6",
          100: "#fbb9bb",
          200: "#f38d90",
          300: "#ec5f65",
          400: "#e53339", // Primary
          500: "#cc1a1f",
          600: "#a01217",
          700: "#720a10",
          800: "#470507",
          900: "#1f0000",
        },
      },
      transitionProperty: {
        height: "height",
        width: "width",
      },
      zIndex: {
        "header-1": 20, // Header level 1
        "sidebar-1": 19, // Sidebar level 1

        "header-2": 18, // Header level 2 (FilterHeader)
        "sidebar-2": 17, // Sidebar level 2 (MyAccount, Settings Page)

        1: 1,
        9999: 9999,
      },
      height: {
        header: `var(--header-height, ${toPx(HEADER_HEIGHT)})`, // headerHeight
        content: `calc(100vh - var(--header-height, ${toPx(HEADER_HEIGHT)}))`,
      },
      width: {
        "screen-3/4": "75vw",
        "screen-1/2": "50vw",
        "1/10": "10%",

        sidebar: toPx(SIDEBAR_WIDTH),
        "sidebar-compact": toPx(SIDEBAR_COMPACT_WIDTH),
        "header-company": toPx(SIDEBAR_WIDTH),
        "header-company-name": toPx(SIDEBAR_WIDTH - 24 - 16), // sidebarWidth - (paddingLeft[16px] + logoWidth[24px])

        "sidebar-space-r": toPx(SIDEBAR_WIDTH + 16), // sidebarWidth + paddingRight[16px]
        "sidebar-space-r-compact": toPx(SIDEBAR_COMPACT_WIDTH + 16), // sidebarCompactWidth + paddingRight[16px]
      },
      maxWidth: {
        "header-company": toPx(SIDEBAR_WIDTH),
        "header-company-name": toPx(SIDEBAR_WIDTH - 24 - 16), // sidebarWidth - (paddingLeft[16px] + logoWidth[24px])
      },
      padding: {
        sidebar: toPx(SIDEBAR_WIDTH), // paddingLeft: sidebarWidth
        "sidebar-compact": toPx(SIDEBAR_COMPACT_WIDTH), // paddingLeft: sidebarCompactWidth

        "6px": "6px",
      },
      minHeight: (theme) => ({
        ...theme("height"),
      }),
      minWidth: (theme) => ({
        ...theme("width"),
      }),
      backgroundImage: () => ({
        "bg-pattern": "url('/images/background.png')",
      }),
      inset: {
        notice: "var(--notice-height, 0px)",
        header: `calc(var(--notice-height, 0px) + var(--header-height, ${toPx(
          HEADER_HEIGHT
        )}))`, // headerHeight
        sidebar: toPx(SIDEBAR_WIDTH), // sidebarWidth
      },
      cursor: {
        "row-resize": "row-resize",
        copy: "copy",
      },
    },
  },
  variants: {
    extend: {
      padding: ["first", "important"],
      backgroundColor: ["checked", "important", "hover:important"],
      borderWidth: ["last", "important"],
      borderColor: ["checked", "important", "hover:important"],
      zIndex: ["important"],
      width: ["important"],
      height: ["important"],
      minWidth: ["important"],
      maxWidth: ["important"],
      minHeight: ["important"],
      maxHeight: ["important"],
      flex: ["important"],
      flexDirection: ["important"],
      alignItems: ["important"],
      justifyContent: ["important"],
      textColor: ["important"],
      cursor: ["important"],
      display: ["hover", "important"],
      fontSize: ["important"],
      outline: ["focus-visible"],
      borderRadius: ["important"],
    },
    opacity: ({ after }) => after(["disabled"]),
  },
  plugins: [
    plugin(function ({ addVariant }) {
      addVariant("important", ({ container }) => {
        container.walkRules((rule) => {
          rule.selector = `.\\!${rule.selector.slice(1)}`;
          rule.walkDecls((decl) => {
            decl.important = true;
          });
        });
      });
      addVariant("hover:important", ({ container }) => {
        container.walkRules((rule) => {
          rule.selector = `.hover\\:\\!${rule.selector.slice(1)}:hover`;
          rule.walkDecls((decl) => {
            decl.important = true;
          });
        });
      });
      addVariant("focus:important", ({ container }) => {
        container.walkRules((rule) => {
          rule.selector = `.focus\\:\\!${rule.selector.slice(1)}:focus`;
          rule.walkDecls((decl) => {
            decl.important = true;
          });
        });
      });
    }),
  ],
};
