import i18n from "i18next";
import { initReactI18next } from "react-i18next";

import labelEN from "./public/locales/en/label.json";
import messageEN from "./public/locales/en/message.json";
import settingEN from "./public/locales/en/setting.json";
import translationEN from "./public/locales/en/translation.json";
import labelVI from "./public/locales/vi/label.json";
import messageVI from "./public/locales/vi/message.json";
import settingVI from "./public/locales/vi/setting.json";
import translationVI from "./public/locales/vi/translation.json";
import { buildYupLocale } from "./yup";

export const defaultNS = "translation";
export const resources = {
  en: {
    translation: translationEN,
    setting: settingEN,
    label: labelEN,
    message: messageEN,
  },

  vi: {
    translation: translationVI,
    setting: settingVI,
    label: labelVI,
    message: messageVI,
  },
} as const;

i18n.use(initReactI18next).init(
  {
    lng: "en",
    ns: ["translation", "setting", "label", "message"],
    defaultNS,
    resources,
  },
  buildYupLocale
);

export default i18n;
