// import "tailwindcss/tailwind.css";
import "rc-drawer/assets/index.css";
import "react-toastify/dist/ReactToastify.css";
import "styles/tailwind.css";
import "../../public/fonts/FontAwesomePro/css/fontawesome.min.css";
import "../../public/fonts/FontAwesomePro/css/solid.min.css";
import "../../public/fonts/FontAwesomePro/css/regular.min.css";
import "../../i18n";
import "animate.css";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import "@hassanmojab/react-modern-calendar-datepicker/lib/DatePicker.css";

import { Global } from "@emotion/react";
import type { AppProps } from "next/app";
import Head from "next/head";
import NextNprogress from "nextjs-progressbar";
import { ToastContainer } from "react-toastify";
import { css, GlobalStyles } from "twin.macro";

const globalStyles = css`
  html,
  body {
    padding: 0;
    margin: 0;
    font-family: "Inter", -apple-system, BlinkMacSystemFont, Segoe UI, Roboto,
      Oxygen, Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue,
      sans-serif;
    background-color: #f5f5f5; // bg-gray-50
  }
  * {
    box-sizing: border-box;
  }
`;

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <>
      <Head>
        <title>
          Nextjs App with TypeScript, ESlint, Jest, Emotion, Tailwind and Twin
        </title>
        <link rel="icon" href="/favicon.ico" />
        {/* <link
          rel="stylesheet"
          type="text/css"
          href="/fonts/FontAwesomePro/css/all.min.css"
        /> */}
      </Head>
      <GlobalStyles />
      <Global styles={globalStyles} />

      <NextNprogress
        color="#f37f26"
        startPosition={0.3}
        stopDelayMs={200}
        height={2}
      />

      <Component {...pageProps} />
      <ToastContainer position="top-center" />
    </>
  );
}

// Only uncomment this method if you have blocking data requirements for
// every single page in your application. This disables the ability to
// perform automatic static optimization, causing every page in your app to
// be server-side rendered.

// MyApp.getInitialProps = async (appContext: AppContext) => {
//   const rootStore = initializeStore(undefined, appContext.ctx);

//   let auth = false;

//   const token = Api.getToken();

//   if (!token) {
//     rootStore.authStore.setAuth(false);
//   } else {
//     const user = await rootStore.authStore.getProfile();

//     if (user) {
//       rootStore.authStore.setAuth(true);
//       auth = true;
//     } else rootStore.authStore.setAuth(false);
//   }

//   await Promise.all([
//     rootStore.companyStore.getCompanies(),
//     rootStore.teamStore.getTeams(),
//   ]).then(() => {
//     rootStore.companyStore.initSelectCompany();
//   });

//   // calls page's `getInitialProps` and fills `appProps.pageProps`
//   appContext.ctx.store = rootStore;
//   const appProps: AppInitialProps = await App.getInitialProps(appContext);

//   utilRedirectAuth(auth, appContext?.ctx);

//   return {
//     pageProps: {
//       ...appProps.pageProps,
//       initialState: getSnapshot(rootStore),
//     },
//   };
// };

export default MyApp;
