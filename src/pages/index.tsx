import { observer } from "mobx-react-lite";
import { NextPage } from "next";
import React from "react";

import { LayoutBase } from "~/components/layout/base";
import { HomeContainer } from "~/containers";

const IndexPage: NextPage = observer(() => {
  return (
    <LayoutBase>
      <HomeContainer />
    </LayoutBase>
  );
});

IndexPage.getInitialProps = async () => {
  // context.store.authStore.setAuth(true);
  return { name: "Charlie" };
};

export default IndexPage;
