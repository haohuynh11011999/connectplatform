import Link from "next/link";
import React from "react";

import { Button } from "~/components";

type BoxItemProps = {
  title: string;
  description: string;
  href: string;
};

const BoxItem: React.FC<BoxItemProps> = ({ title, description, href }) => (
  <div className="flex-grow w-full p-2 md:w-1/2">
    <Link href={href} passHref>
      <div className="px-6 py-4 border border-gray-200 border-solid cursor-pointer rounded-md hover:bg-gray-100 hover:text-blue-600">
        <h1 className="mb-3 text-2xl leading-normal">{title} →</h1>
        <p className="text-base">{description}</p>
      </div>
    </Link>
  </div>
);

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export default function Tailwind() {
  return (
    <div className="flex flex-col max-w-3xl p-4 mx-auto my-10 bg-white shadow-lg md:p-12 rounded-md">
      <h1 className="text-6xl leading-normal text-center">
        Welcome to{" "}
        <Link href="/">
          <a className="text-blue-600 hover:underline">Tailwind CSS!</a>
        </Link>
      </h1>
      <p className="text-2xl leading-normal text-center">
        Get started by editing{" "}
        <code className="p-1 bg-gray-100 rounded-lg">pages/index.tsx</code>
      </p>
      <div className="flex flex-row flex-wrap mt-10 -m-2">
        <BoxItem
          title="Documentation"
          description="Find in-depth information about Next.js features and API."
          href="/"
        />

        <BoxItem
          title="Learn"
          description="Learn about Next.js in an interactive course with quizzes!"
          href="/"
        />

        <BoxItem
          title="Examples"
          description="Discover and deploy boilerplate example Next.js projects."
          href="/"
        />

        <BoxItem
          title="Examples"
          description="Instantly deploy your Next.js site to a public URL with Vercel."
          href="/"
        />
        <div className="w-full space-y-4">
          <Button
            title="Đây là title small"
            size="small"
            type="default"
            onClick={() => console.log("📢 d")}
            loading
            block
          />
          <Button
            title="Đây là title"
            size="default"
            type="default"
            onClick={() => console.log("📢 d")}
            loading
            block
          />
          <Button
            title="Đây là title"
            size="large"
            type="default"
            onClick={() => console.log("📢 d")}
            loading
            block
          />
          <Button
            title="Đây là title"
            size="large"
            type="default"
            onClick={() => console.log("📢 d")}
            loading
            block
          />
          <Button
            title="Đây là title"
            size="small"
            type="primary"
            onClick={() => console.log("📢 d")}
            loading
            block
          />
          <Button
            title="Đây là title"
            size="default"
            type="primary"
            onClick={() => console.log("📢 d")}
            loading
            block
          />
          <Button
            title="Đây là title"
            size="large"
            type="primary"
            onClick={() => console.log("📢 d")}
            loading
            block
          />
        </div>
      </div>
    </div>
  );
}
