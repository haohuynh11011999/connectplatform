import { ApisauceInstance, create } from "apisauce";

import { ApiConfig } from "./api-config";

class Api {
  private static instance: ApisauceInstance;
  private static token: string;
  private static tenantId: string;

  public static getInstance(): ApisauceInstance {
    if (!Api.instance) {
      Api.instance = create({ baseURL: ApiConfig().gateway });

      Api.instance.axiosInstance.interceptors.request.use(
        (request) => {
          request.headers = {
            Accept: "application/json",
            Authorization: "Bearer " + Api.token,
            "tenant-id": Api.tenantId,
          };
          return request;
        },
        (error) => {
          return error;
        }
      );
      Api.instance.axiosInstance.interceptors.response.use(
        (response) => {
          return response;
        },
        (error) => {
          return error;
        }
      );
    }
    return Api.instance;
  }

  public static getToken() {
    return Api.token;
  }

  public static getTenantId() {
    return Api.tenantId;
  }
}

export { Api };
