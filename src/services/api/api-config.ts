import envGlobal from "~/../env";

type ApiConfigType = {
  gateway: string;
  auth: string;
  core: string;
  drive: string;
  company: string;
};

const DevelopmentConfigServices = {
  auth: "http://localhost:5024",
  core: "http://localhost:5023",
  drive: "http://localhost:5000",
  company: "http://localhost:5022",
};

export const ApiConfig = (): ApiConfigType => {
  const {
    WEB_GATEWAY_API,
    DEVELOPMENT_SERVICES,
    BASEURL_API_AUTH,
    BASEURL_API_DRIVE,
    BASEURL_API_COMPANY,
    BASEURL_API_EOS_CORE,
    INTERNAL_API_AUTH,
    INTERNAL_API_COMPANY,
    INTERNAL_API_DRIVE,
    INTERNAL_API_EOS_CORE,
  } = envGlobal();
  const gateway = WEB_GATEWAY_API;

  const getService = (service: string, internal: string): string => {
    if (process.browser) return [gateway, service].join("/");
    return internal;
  };
  const services = {
    gateway,
    auth: getService(BASEURL_API_AUTH, INTERNAL_API_AUTH),
    core: getService(BASEURL_API_EOS_CORE, INTERNAL_API_EOS_CORE),
    drive: getService(BASEURL_API_DRIVE, INTERNAL_API_DRIVE),
    company: getService(BASEURL_API_COMPANY, INTERNAL_API_COMPANY),
  };
  const developmentServices = (DEVELOPMENT_SERVICES ?? "").split(",");

  if (developmentServices.length > 0) {
    developmentServices.forEach((service) => {
      services[service] = DevelopmentConfigServices[service];
    });
  }

  return services;
};
