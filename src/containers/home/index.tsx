/* eslint-disable prettier/prettier */
import { Button } from "~/components";

import { dateFake } from "././constant";
import {
  BlogItem,
  ConnectBox,
  FilterTool,
  Pagination,
  ProjectItem,
  TeacherItem,
} from "./component";
import { ConnectItem } from "./component/connect-box/ConnecItem";
import { ProjectSlide } from "./component/project-slide";
export const HomeContainer = () => {
  return (
    <>
      <div
        className="w-full h-full bg-cover bg-bg-pattern"
        style={{ height: "600px" }}
      >
        <div className="h-full grid grid-cols-2">
          <div></div>
          <div className="flex flex-col items-center justify-center h-full font-medium text-white space-y-8">
            <h2 className="text-2xl">NƠI KHƠI NGUỒN SỰ KẾT NỐI</h2>

            <Button title="Tạo dự án ngay" type="primary" />
          </div>
        </div>
      </div>

      <div className="mt-8">
        <div className="container">
          <div className="text-xl font-medium text-center">
            <h2>CÁC DỰ ÁN ĐƯỢC YÊU THÍCH NHẤT</h2>
          </div>

          <div className=" mt-4 bg-white shadow-md rounded-md">
            <ProjectSlide />
          </div>
        </div>
      </div>

      <div className="container mt-8 space-y-4">
        <h2 className="text-xl font-medium text-center">Dự án</h2>
        <FilterTool />
      </div>

      <div className="mx-8 mt-8">
        <div className="grid grid-cols-12 gap-4">
          <div className="items-center justify-center col-span-9 grid grid-cols-3 gap-4">
            {dateFake.map((data, index) => (
              <ProjectItem
                key={index}
                fullName={data.fullName}
                major={data.major}
                title={data.title}
                description={data.description}
                date={data.date}
              />
            ))}
          </div>

          <div className="col-span-3 space-y-4">
            <ConnectBox title="Kết nối với cựu sinh viên">
              <>
                <ConnectItem />
                <ConnectItem />
                <ConnectItem />
                <ConnectItem />
              </>
            </ConnectBox>
            <ConnectBox title="Các đề tài tiêu biểu">
              <>
                <ConnectItem />
                <ConnectItem />
                <ConnectItem />
                <ConnectItem />
              </>
            </ConnectBox>

            <ConnectBox title="Blogs chia sẻ nghiên cứu khoa học">
              <>
                <BlogItem
                  title="1. Chia se bi quyet tham gia Nghien Cuu Khoa Hoc"
                  author="Nguyen Van A"
                />
                <BlogItem
                  title="2. Chia se bi quyet tham gia Nghien Cuu Khoa Hoc"
                  author="Nguyen Van A"
                />
                <BlogItem
                  title="3. Chia se bi quyet tham gia Nghien Cuu Khoa Hoc"
                  author="Nguyen Van A"
                />
                <BlogItem
                  title="4. Chia se bi quyet tham gia Nghien Cuu Khoa Hoc"
                  author="Nguyen Van A"
                />
              </>
            </ConnectBox>
          </div>
        </div>
      </div>

      <div className="mt-8">
        <div className="flex items-center justify-center">
          <Pagination />
        </div>
      </div>

      <div className="my-8">
        <div className="container space-y-4">
          <div className="text-xl font-medium text-center">
            <h2>GIẢNG VIÊN TIÊU BIỂU</h2>
          </div>

          <div className="flex items-center space-x-4">
            <TeacherItem
              fullName="Lê Hoành Sử"
              title="Trưởng khoa HTTT"
              level="Tiến sĩ"
              email="sulh@uel.edu.vn"
            />

            <TeacherItem
              fullName="Trần Thị Ánh"
              title="Trưởng bộ môn"
              level="Thạc sĩ"
              email="anhtt@uel.edu.vn"
            />

            <TeacherItem
              fullName="Nguyễn Quang Hưng"
              title="Nghiên cứu sinh"
              level="Thạc sĩ"
              email="hungnq@uel.edu.vn"
            />

            <TeacherItem
              fullName="Nguyễn Quang Phúc"
              title="Giảng viên"
              level="Thạc sĩ"
              email="phucnq@uel.edu.vn"
            />

            <TeacherItem
              fullName="Trần Thị Ánh"
              title="Trưởng bộ môn"
              level="Thạc sĩ"
              email="anhtt@uel.edu.vn"
            />
            <TeacherItem
              fullName="Trần Thị Ánh"
              title="Trưởng bộ môn"
              level="Thạc sĩ"
              email="anhtt@uel.edu.vn"
            />
          </div>
        </div>
      </div>
    </>
  );
};
