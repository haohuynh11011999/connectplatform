/* eslint-disable prettier/prettier */
type IProps = {
  title: string;
  author: string;
};
export const BlogItem: React.FC<IProps> = ({ title, author }) => {
  return (
    <div className="flex flex-col cursor-pointer space-y-2 hover:text-primary-400 transition-all duration-300 ease-in-out">
      <h2 className="font-medium ">{title}</h2>
      <span className="text-gray-400">Tác giả: {author}</span>
    </div>
  );
};
