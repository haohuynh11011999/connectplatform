/* eslint-disable prettier/prettier */
import { Input, SelectComplex } from "~/components";

import { LIST_JOBS } from "../../constant";

export const FilterTool = () => {
  return (
    <div className="flex flex-col items-center justify-center space-y-4">
      <div className="flex items-center justify-center space-x-8">
        <div className="w-96 col-span-1 grid grid-cols-2 gap-4">
          <SelectComplex
            options={[]}
            name="major"
            selected={null}
            placeholder="Khoa"
          />
          <SelectComplex
            options={[]}
            name="major"
            selected={null}
            placeholder="Lĩnh vực"
          />
        </div>

        <div className="col-span-2">
          <Input
            prefix={<i className="fas fa-search" />}
            placeholder="Tìm kiếm đề tài"
            type="default"
          />
        </div>
      </div>

      <div className="overflow-auto">
        <div className="flex items-center" style={{ maxWidth: 768 }}>
          <div className="flex items-center p-2 space-x-4">
            {LIST_JOBS.map((job, index) => (
              <div
                key={index}
                className="w-40 px-4 py-2 text-center text-black bg-white shadow-md rounded-md"
              >
                <span>{job}</span>
              </div>
            ))}
          </div>
        </div>
      </div>
    </div>
  );
};
