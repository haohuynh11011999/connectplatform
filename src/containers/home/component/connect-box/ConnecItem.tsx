/* eslint-disable prettier/prettier */
import { Avatar, Button } from "~/components";
export const ConnectItem = () => {
  return (
    <div className="flex flex-col items-start space-y-2">
      <div className="flex space-x-4">
        <span>
          <Avatar src="" fullName="Nguyen Thi Anh" size="medium" />
        </span>

        <div className="flex flex-col space-y-2">
          <div>
            <h2 className="font-medium">Nguyen Van A</h2>
            <span className="text-xs text-gray-400">
              Developer at tungtung.vn
            </span>
          </div>

          <div>
            <Button title="Kết nối" size="small" />
          </div>
        </div>
      </div>
    </div>
  );
};
