/* eslint-disable prettier/prettier */

type IProps = {
  title: string;
  children?: JSX.Element;
};
export const ConnectBox: React.FC<IProps> = ({ title, children }) => {
  return (
    <div className="mx-4 bg-white border-2 rounded-md">
      <div className="border-b">
        <div className="px-4 py-2 font-medium">
          <h2>{title}</h2>
        </div>
      </div>

      <div className="px-4 py-2 overflow-auto space-y-4">
        {children}

        <div className="flex items-center font-medium text-gray-400 space-x-2">
          <span>Xem thêm đề xuất</span>
          <i className="fas fa-arrow-right" />
        </div>
      </div>
    </div>
  );
};
