/* eslint-disable prettier/prettier */
import { Avatar, Button } from "~/components";

import { Illustration } from "./illustation";

type IProps = {
  fullName: string;
  major: string;
  title: string;
  description: string;
  date: string;
};
export const ProjectItem: React.FC<IProps> = ({
  fullName,
  major,
  title,
  description,
  date,
}) => {
  return (
    <div className="bg-white shadow-md cursor-pointer w-80 rounded-md group">
      <div className="w-full border-b bg-primary-50">
        <div className="flex items-center justify-between p-2">
          <div className="flex items-center space-x-2">
            <Avatar src="" fullName="Huynh Nhat Hao" />

            <div className="flex flex-col">
              <span className="font-medium">{fullName}</span>
              <span className="text-xs">{major}</span>
            </div>
          </div>
          <div>
            <Button title="Tham gia" type="primary" size="small" />
          </div>
        </div>
      </div>

      <div className="border-b ">
        <div className="flex flex-col p-2 space-y-2">
          <Illustration />

          <h2 className="font-semibold text-center group-hover:text-primary-400 duration-300 transition-all ease-in-out">
            {title}
          </h2>

          <p className="text-center">{description}</p>
        </div>
      </div>

      <div className="flex items-center justify-between p-2">
        <div className="flex items-center space-x-2">
          <i className="fas fa-thumbs-up" />
          <span>Thich</span>
        </div>

        <span>{date}</span>
      </div>
    </div>
  );
};
