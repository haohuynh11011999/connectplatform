/* eslint-disable prettier/prettier */
import { Avatar, Button } from "~/components";
type IProps = {
  fullName: string;
  email: string;
  level: string;
  title: string;
};
export const TeacherItem: React.FC<IProps> = ({
  fullName,
  email,
  level,
  title,
}) => {
  return (
    <div className="flex flex-col w-56 p-4 bg-white shadow-md h-80 rounded-md space-y-2">
      <div className="flex items-center justify-center">
        <Avatar
          className="rounded-full !w-24 !h-24"
          src=""
          fullName={fullName}
        />
      </div>

      <h2 className="text-base font-medium text-center">{fullName}</h2>

      <div className="flex flex-col flex-1 text-gray-400 space-y-2">
        <span>{title}</span>
        <span>Học vị: {level}</span>
        <span>Email: {email}</span>
      </div>

      <div className="flex items-center justify-center">
        <Button title="Kết nối" size="small" />
      </div>
    </div>
  );
};
