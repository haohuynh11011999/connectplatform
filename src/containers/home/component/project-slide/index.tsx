/* eslint-disable prettier/prettier */
import classNames from "classnames";
import Slider from "react-slick";

import { ProjectItem } from "..";
import { dateFake } from "./../../constant";

export const ProjectSlide = () => {
  const settings = {
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 3,
    arrows: true,
    autoPlay: true,
    centerMode: true,
  };
  return (
    <Slider {...settings}>
      {dateFake.map((data, index) => (
        <div className="px-2 py-4" key={index}>
          <ProjectItem
            fullName={data.fullName}
            major={data.major}
            title={data.title}
            description={data.description}
            date={data.date}
          />
        </div>
      ))}
    </Slider>
  );
};
