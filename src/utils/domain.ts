import { NextPageContext } from "next";
import Router from "next/router";

import envGlobal from "~/../env";
import { ROUTES_AUTH } from "~/constants";

export function getSubDomain() {
  const { NODE_ENV, TENANT_ID } = envGlobal();

  if (NODE_ENV !== "production") return TENANT_ID;

  return window.location.host.split(".")[0] ?? "";
}

export function utilGetTenantId(host?: string) {
  if (typeof window === "undefined") {
    if (!host) return "";
    return host.split(".")[0];
  }

  return window.location.hostname.split(".")[0];
}

export function utilRedirectLocation(location: string, ctx?: NextPageContext) {
  if (typeof window === "undefined") {
    ctx?.res?.writeHead(302, {
      location,
    });
    ctx?.res?.end();
  } else {
    Router.replace(location);
  }
}

export function utilRedirectAuth(isAuth: boolean, ctx?: NextPageContext) {
  const url = ctx?.req?.url;
  if (!isAuth) {
    if (!ROUTES_AUTH.includes(url)) {
      utilRedirectLocation(ROUTES_AUTH[0], ctx);
    }
  } else {
    if (ROUTES_AUTH.includes(url)) {
      utilRedirectLocation("/", ctx);
    }
  }
}
