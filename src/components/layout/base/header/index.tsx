import React from "react";

import { BellIcon } from "~/assets/icons/Bell";
import { Button } from "~/components";
import { getCSSVar } from "~/utils/cssVar";

export const Header: React.FC = () => {
  const headerHeight = getCSSVar("header-height", "56px");
  return (
    <header
      className=" fixed z-20 flex items-center w-screen bg-white shadow-md  d-flex"
      style={{ height: headerHeight }}
    >
      <div className="container flex items-center">
        <div className="flex flex-1 logo">
          <h1>Logo</h1>
        </div>

        <div className="flex h-16 mr-8">
          <ul className="flex flex-row items-center h-full list-none space-x-4">
            <li className="px-4 py-2 font-medium bg-primary-50 text-primary-400 rounded-md">
              <a>Giới thiệu</a>
            </li>

            <li>
              <a>Hộp thư</a>
            </li>

            <li>
              <a>Tài liệu</a>
            </li>
          </ul>
        </div>

        <div className="flex items-center space-x-4">
          <span className="flex items-center justify-center w-8 h-8 text-gray-400 bg-gray-100 rounded-full shadow-inner cursor-pointer hover:bg-gray-200">
            <i className="fas fa-user-plus" />
          </span>
          <span className="flex items-center justify-center w-8 h-8 text-gray-400 bg-gray-100 rounded-full shadow-inner cursor-pointer hover:bg-gray-200">
            <BellIcon size={24} />
          </span>
          <Button title="Đăng nhập" type="primary" size="small" />
        </div>
      </div>
    </header>
  );
};
