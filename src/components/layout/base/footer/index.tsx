/* eslint-disable prettier/prettier */
export const Footer = () => {
  return (
    <footer
      className=" flex items-center shadow-md bg-primary-100 d-flex"
      style={{ height: "80px" }}
    >
      <div className="container flex items-center">
        <div className="flex flex-1 logo">
          <h1>Logo</h1>
        </div>

        <div className="flex h-full">
          <ul className="flex flex-row items-center h-full font-medium list-none space-x-4">
            <li>
              <a>Về chúng tôi</a>
            </li>

            <li>
              <a>Chính sách & Điều khoản</a>
            </li>

            <li>
              <a>Liên hệ</a>
            </li>
          </ul>
        </div>
      </div>
    </footer>
  );
};
