/* eslint-disable prettier/prettier */
import NextHead from "next/head";
import { useRouter } from "next/router";

import envGlobal from "~/../env";

type IProps = {
  pageTitle?: string;
};

export const Head: React.FC<IProps> = ({ pageTitle }) => {
  const router = useRouter();

  const { WEB_GATEWAY_API } = envGlobal();

  const title = pageTitle ? `${pageTitle}` : "";
  const url = WEB_GATEWAY_API + router.asPath;
  const description = "EOS Software with Powerful";
  const fbAppId = "1527529787437950";
  const siteCoverImageRectangle =
    WEB_GATEWAY_API + "/images/site-cover-image-rectangle.jpg";
  const siteCoverImageSquare =
    WEB_GATEWAY_API + "/images/site-cover-image-square.jpg";

  return (
    <NextHead>
      <title>{title}</title>
      {/* <meta name="theme-color" content="#f37f26" /> */}
      <meta name="description" content={description} />
      <meta name="keywords" content="tractionwork, eos" />
      <meta name="author" content="tungtung" />
      <meta property="fb:app_id" content={fbAppId} />
      {/* Open Graph */}
      <meta property="og:url" content={url} />
      <meta property="og:type" content="website" />
      <meta property="og:title" content={title} />
      <meta property="og:description" content={description} />
      <meta property="og:image" content={siteCoverImageRectangle} />
      <meta property="og:image:width" content="1200" />
      <meta property="og:image:height" content="630" />
      {/* Twitter Card */}
      <meta name="twitter :card" content="summary" />
      <meta name="twitter:title" content={title} />
      <meta name="twitter:description" content={description} />
      <meta name="twitter:creator" content="@tungtung" />
      <meta name="twitter:image" content={siteCoverImageSquare} />
    </NextHead>
  );
};
