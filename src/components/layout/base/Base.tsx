import { observer } from "mobx-react-lite";
import { useRouter } from "next/dist/client/router";
import React from "react";

import { getCSSVar } from "~/utils/cssVar";

import { Footer } from "./footer";
import { Head } from "./head";
import { Header } from "./header";

type IProps = {
  pageTitle?: string;
  children: JSX.Element | JSX.Element[];
};

export const LayoutBase: React.FC<IProps> = observer(
  ({ pageTitle, children }) => {
    const headerHeight = getCSSVar("header-height", "56px");

    return (
      <>
        <Head />
        <Header />
        <div style={{ height: headerHeight }}></div>

        <div className="">
          <main>{children}</main>
        </div>

        <Footer />
      </>
    );
  }
);
