import classNames from "classnames";
import { memo } from "react";
import { useEffect } from "react";
import { useState } from "react";

import { Collapse as BaseCollapse } from "../collapse";
import { Tooltip } from "../tooltip";

type IIssuesProps = {
  label: string;
  children: JSX.Element;
  defaultOpened?: boolean;
  isRequired?: boolean;
  isNoSpace?: boolean;
};

const _FormItemCollapse: React.FC<IIssuesProps> = ({
  label,
  defaultOpened,
  isRequired,
  isNoSpace,
  children,
}) => {
  const [isOpened, setIsOpened] = useState(defaultOpened);

  useEffect(() => {
    setIsOpened(defaultOpened);
  }, [defaultOpened]);

  return (
    <div className={classNames({ "mb-4": !isNoSpace })}>
      <Tooltip title="Collapse/Expand">
        <button
          type="button"
          onClick={() => setIsOpened((v) => !v)}
          className="font-medium text-left focus:outline-none min-w-20"
        >
          <i
            className={classNames(
              "fas fa-caret-right transition-all duration-300 mr-2",
              {
                "transform rotate-90": isOpened,
              }
            )}
          />
          <span className="flex-1">
            {label}{" "}
            {isRequired ? <span className="text-red-400">*</span> : null}
          </span>
        </button>
      </Tooltip>

      <BaseCollapse isOpened={isOpened}>
        <div className="pt-2">{children}</div>
      </BaseCollapse>
    </div>
  );
};

export const FormItemCollapse = memo(_FormItemCollapse);
