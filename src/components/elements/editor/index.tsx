import dynamic from "next/dynamic";

export const Editor = dynamic(() => import("./Editor"), {
  ssr: false,
  // eslint-disable-next-line react/display-name
  loading: () => <div className="w-full">Loading ...</div>,
});
