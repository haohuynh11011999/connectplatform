import classNames from "classnames";

type IPropsModalHeader = {
  title?: string;
  customHeader: React.ReactNode;
  onCancel?: () => void;
  showHeader: boolean;
};

export const ModalHeader: React.FC<IPropsModalHeader> = ({
  title,
  customHeader,
  showHeader,
  onCancel,
}) => {
  return (
    <div className={classNames("modal-header", !showHeader ? "hidden" : "")}>
      <div className="flex items-baseline flex-1 space-x-4">
        <h3 className="text-base font-medium">{title}</h3>
        {customHeader && <>{customHeader}</>}
      </div>

      <button
        className="flex items-center p-1 text-lg text-gray-300 bg-white rounded-full cursor-pointer hover:text-gray-400 focus:outline-none"
        onClick={onCancel}
      >
        <i className="fas fa-times" />
      </button>
    </div>
  );
};
