import { Dialog, Transition } from "@headlessui/react";
import classNames from "classnames";
import React, { Fragment, useRef } from "react";
import { useMemo } from "react";

import { BREAKPOINTS } from "~/constants";
import useWindowSize from "~/hooks/useWindowSize";

import { Button } from "../button";
import { ModalHeader } from "./ModalHeader";

type IProps = {
  visible: boolean;
  children?: React.ReactNode;
  title?: string;
  customHeader?: React.ReactNode;
  onCancel: () => void;
  onOk?: () => void;
  okText?: string;
  cancelText?: string;
  disable?: boolean;
  loading?: boolean;
  className?: string;
  width?: string | number;
  showHeader?: boolean;
  minHeight?: string | number;
  isOverflowAuto?: boolean;
};

const Modal: React.FC<IProps> = ({
  visible = false,
  children,
  title = "Basic Modal",
  customHeader,
  onCancel,
  onOk,
  okText = "Save",
  cancelText = "Cancel",
  disable = false,
  loading = false,
  className,
  width,
  showHeader = true,
  minHeight,
  isOverflowAuto = true,
}) => {
  const cancelButtonRef = useRef(null);
  const size = useWindowSize();

  const modalWidth = useMemo(() => {
    if (size.width < BREAKPOINTS.sm) {
      return "100%";
    }

    return width || 640;
  }, [size.width, width]);

  return (
    <Transition.Root show={visible} as={Fragment}>
      <Dialog
        as="div"
        static
        className={classNames(
          "fixed inset-0 overflow-y-auto z-9999",
          className
        )}
        initialFocus={cancelButtonRef}
        open={visible}
        onClose={onCancel}
      >
        <div className="items-center justify-center min-h-screen px-4 pt-4 pb-20 text-center !flex sm:block">
          <Transition.Child
            as={Fragment}
            enter="ease-out duration-300"
            enterFrom="opacity-0"
            enterTo="opacity-100"
            leave="ease-in duration-300"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <Dialog.Overlay className="fixed inset-0 bg-black bg-opacity-30 transition-opacity" />
          </Transition.Child>

          <Transition.Child
            as={Fragment}
            enter="ease-out duration-300"
            enterFrom="opacity-0 scale-0 sm:scale-95"
            enterTo="opacity-100 scale-100 sm:scale-100"
            leave="ease-in duration-200"
            leaveFrom="opacity-100 sm:scale-100"
            leaveTo="opacity-0 sm:scale-95"
          >
            <div
              style={{
                width: modalWidth,
                minHeight,
                marginTop: 16,
                marginBottom: 16,
              }}
              className={classNames("modal", {
                "overflow-hidden": isOverflowAuto,
              })}
            >
              <ModalHeader
                showHeader={showHeader}
                title={title}
                onCancel={onCancel}
                customHeader={customHeader}
              />

              <div
                className={classNames("modal-body", {
                  "overflow-auto": isOverflowAuto,
                })}
              >
                {children}
              </div>

              {onOk && (
                <div className="modal-footer">
                  <Button
                    title={cancelText}
                    type="default"
                    onClick={onCancel}
                    className="min-w-24"
                  />

                  <Button
                    loading={loading}
                    disabled={disable}
                    title={okText}
                    type="primary"
                    onClick={onOk}
                    className="min-w-24"
                  />
                </div>
              )}
            </div>
          </Transition.Child>
        </div>
      </Dialog>
    </Transition.Root>
  );
};

export { Modal };
