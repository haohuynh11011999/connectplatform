import { useRouter } from "next/router";

type IProps = {
  children: JSX.Element;
  userId: string;
  disable?: boolean;

  onMouseEnter?: React.MouseEventHandler;
  onMouseLeave?: React.MouseEventHandler;
  onFocus?: React.FocusEventHandler;
};
export const WrapLinkToProfile: React.FC<IProps> = ({
  children,
  userId,
  disable = false,

  onMouseEnter,
  onMouseLeave,
  onFocus,
}) => {
  const router = useRouter();

  const handleClick = (e: React.MouseEvent | React.KeyboardEvent) => {
    if (disable || !userId) {
      return;
    }

    e.stopPropagation();

    router.push({
      pathname: "/people/[id]",
      query: {
        id: userId,
      },
    });
  };

  return (
    <span
      className="cursor-pointer"
      role="button"
      tabIndex={0}
      //
      onMouseEnter={onMouseEnter}
      onMouseLeave={onMouseLeave}
      onFocus={onFocus}
      onClick={handleClick}
      onKeyPress={handleClick}
    >
      {children}
    </span>
  );
};
