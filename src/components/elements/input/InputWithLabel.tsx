import classNames from "classnames";
import React, { InputHTMLAttributes } from "react";

type IProps = InputHTMLAttributes<HTMLInputElement> & {
  name: string;
  label: string;
  placeholder: string;
  error?: string;
  suffix?: JSX.Element;
  prefix?: JSX.Element;
};

export const InputWithLabel = React.forwardRef<HTMLInputElement, IProps>(
  ({ label, placeholder, name, error, suffix, ...props }, ref) => {
    return (
      <div className="flex flex-col flex-1">
        <label
          htmlFor={name}
          className={classNames(
            "block pb-1 text-base font-semibold text-gray-900"
          )}
        >
          {label}
        </label>
        <div
          className={classNames(
            "flex flex-row items-center",
            "focus:outline-none",
            "bg-gray-100",
            "border rounded focus-within:border-primary-400",
            error ? "border-red-500" : "border-transparent"
          )}
        >
          <input
            ref={ref}
            placeholder={placeholder}
            id={name}
            name={name}
            className={classNames(
              "w-full",
              "px-4 py-3",
              "rounded",
              "focus:outline-none",
              "active:outline-none",
              "bg-transparent",
              props.className
            )}
            {...props}
          />
          {suffix && (
            <div className="flex items-center justify-center mx-4">
              {suffix}
            </div>
          )}
        </div>

        <p className="block pt-1 text-sm text-red-500">{error ? error : ""}</p>
      </div>
    );
  }
);

InputWithLabel.displayName = "Input";
