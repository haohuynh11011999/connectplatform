import { Listbox, Transition } from "@headlessui/react";
import classnames from "classnames";
import { Fragment, useMemo, useState } from "react";

import { useComponentDidUpdate } from "~/hooks";

import { EmptyIcon } from "./icons";

const TYPE_MAPS = {
  default: "select-default",
  outline: "select-outline",
};

const SIZE_MAPS = {
  small: {
    select: "select-sm",
    dropdownItem: "dropdown-item-sm",
  },
  middle: "",
  large: {
    select: "select-lg",
    dropdownItem: "dropdown-item-lg",
  },
};

const KEY = {
  select: "select",
  dropdownItem: "dropdownItem",
};

export type ISelectOption = {
  icon?: JSX.Element | string;
  name: string;
  value: string;
  suffix?: JSX.Element;
};

type IProps = {
  options: ISelectOption[];
  defaultValue?: ISelectOption | string;
  size?: keyof typeof SIZE_MAPS;
  type?: keyof typeof TYPE_MAPS;
  onChange: (key: string) => void;
  label?: string;
  placeholder?: string;
};

export const Select: React.FC<IProps> = ({
  options = [],
  defaultValue,
  size = "middle",
  type = "default",
  label,
  onChange,
  placeholder,
}) => {
  const [selected, setSelected] = useState<ISelectOption>(() => {
    if (typeof defaultValue === "string") {
      return options.find((option) => option.value === defaultValue);
    }

    return defaultValue;
  });

  useComponentDidUpdate(() => {
    onChange(selected?.value);
  }, [selected, onChange]);

  const renderMenuItem = useMemo(() => {
    if (!options?.length) {
      return (
        <Listbox.Options className="flex flex-col items-center justify-center py-4 text-gray-300 dropdown-item">
          <EmptyIcon />
          <span className="mt-2">Empty Data</span>
        </Listbox.Options>
      );
    }

    return (
      <Listbox.Options
        static
        className={classnames(
          "dropdown-item",
          SIZE_MAPS[size][KEY.dropdownItem]
        )}
      >
        {options.map((option) => (
          <Listbox.Option
            key={option.value}
            className={({ active }) =>
              classnames(
                active ? "text-gray-600  bg-gray-100" : "text-gray-900",
                "cursor-pointer select-none relative py-2 px-4"
              )
            }
            value={option}
          >
            {({ selected }) => (
              <>
                <div className="flex items-center">
                  <span
                    className={classnames("block truncate", {
                      "font-medium": selected,
                    })}
                  >
                    {option.name}
                  </span>
                </div>
              </>
            )}
          </Listbox.Option>
        ))}
      </Listbox.Options>
    );
  }, [options, size]);

  return (
    <Listbox value={selected} onChange={setSelected}>
      {({ open }) => (
        <>
          {label && (
            <Listbox.Label className="block mb-2 font-semibold text-gray-900">
              {label}
            </Listbox.Label>
          )}
          <div className="relative">
            <Listbox.Button
              className={classnames(
                "select w-full focus:ring-primary-200 cursor-pointer px-4 open space-x-2",
                SIZE_MAPS[size][KEY.select],
                TYPE_MAPS[type],
                {
                  "ring-1 ring-primary-400": open,
                }
              )}
            >
              {selected?.name ? (
                <span className="block font-normal text-black truncate">
                  {selected.name}
                </span>
              ) : (
                <span className="block font-normal text-gray-400 truncate">
                  {placeholder}
                </span>
              )}
              <span className="text-gray-400 pointer-events-none">
                <i className="fas fa-caret-down" />
              </span>
            </Listbox.Button>
            <Transition
              show={open}
              as={Fragment}
              enter="transition ease-out duration-100"
              enterFrom="transform opacity-0 scale-95"
              enterTo="transform opacity-100 scale-100"
              leave="transition ease-in duration-75"
              leaveFrom="transform opacity-100 scale-100"
              leaveTo="transform opacity-0 scale-95"
            >
              {renderMenuItem}
            </Transition>
          </div>
        </>
      )}
    </Listbox>
  );
};
