import { Listbox, Transition } from "@headlessui/react";
import classnames from "classnames";
import { Fragment, useMemo } from "react";

import { Avatar } from "~/components/elements";

import { ISelectOption } from ".";
import { EmptyIcon } from "./icons";

const TYPE_MAPS = {
  default: "select-default",
  outline: "select-outline",
};

const SIZE_MAPS = {
  small: {
    select: "select-sm",
    dropdownItem: "dropdown-item-sm",
  },
  middle: "",
  large: {
    select: "select-lg",
    dropdownItem: "dropdown-item-lg",
  },
};

const KEY = {
  select: "select",
  dropdownItem: "dropdownItem",
};

type IProps = {
  options: ISelectOption[];
  value?: string;
  size?: keyof typeof SIZE_MAPS;
  type?: keyof typeof TYPE_MAPS;
  onChange: (key: string) => void;
  placeholder?: string;
  isShowIcon?: boolean;
  maxHeight?: number | string;
  isDisabled?: boolean;
  colorValue?: string;
};

export const SelectControlled: React.FC<IProps> = ({
  options = [],
  value,
  size = "middle",
  type = "default",
  onChange,
  placeholder,
  isShowIcon,
  maxHeight,
  isDisabled,
  colorValue,
}) => {
  const selected = options.find((option) => option.value === value);

  const handleChange = (selected: ISelectOption) => {
    if (onChange) {
      onChange(selected?.value);
    }
  };

  const renderMenuItem = useMemo(() => {
    if (!options?.length) {
      return (
        <Listbox.Options className="flex flex-col items-center justify-center py-4 text-gray-300 dropdown-item">
          <EmptyIcon />
          <span className="mt-2">Empty Data</span>
        </Listbox.Options>
      );
    }

    return (
      <Listbox.Options
        static
        className={classnames(
          "dropdown-item",
          SIZE_MAPS[size][KEY.dropdownItem]
        )}
        style={{ maxHeight }}
      >
        {options.map((option) => (
          <Listbox.Option
            key={option.value}
            className={({ active }) =>
              classnames(
                active ? "text-gray-600  bg-gray-100" : "text-gray-900",
                "cursor-pointer select-none relative py-2 px-4"
              )
            }
            value={option}
          >
            {({ selected }) => (
              <>
                <div className="flex items-center space-x-2">
                  {isShowIcon &&
                    (typeof option?.icon === "string" ? (
                      <Avatar
                        src={option.icon}
                        fullName={option.name}
                        size="small"
                      />
                    ) : (
                      option?.icon
                    ))}
                  <span
                    className={classnames("block truncate", {
                      "font-medium": selected,
                    })}
                  >
                    {option.name}
                  </span>
                </div>
              </>
            )}
          </Listbox.Option>
        ))}
      </Listbox.Options>
    );
  }, [options, size, isShowIcon, maxHeight]);

  return (
    <Listbox value={selected} onChange={handleChange} disabled={isDisabled}>
      {({ open }) => (
        <>
          <div className="relative">
            <Listbox.Button
              className={classnames(
                "select w-full px-4 space-x-2",
                SIZE_MAPS[size][KEY.select],
                TYPE_MAPS[type],
                {
                  "ring-1 ring-primary-400": open,
                  "cursor-pointer": !isDisabled,
                  "cursor-not-allowed border opacity-50": isDisabled,
                }
              )}
              disabled={isDisabled}
            >
              {selected?.name ? (
                <div className="flex items-center truncate space-x-2">
                  {isShowIcon &&
                    (typeof selected?.icon === "string" ? (
                      <Avatar
                        src={selected.icon}
                        fullName={selected.name}
                        size="small"
                      />
                    ) : (
                      selected?.icon
                    ))}
                  <span
                    className="block font-normal text-left text-black truncate"
                    style={{ color: colorValue }}
                  >
                    {selected.name}
                  </span>
                </div>
              ) : (
                <span className="block font-normal text-gray-400 truncate">
                  {placeholder}
                </span>
              )}
              <span className="text-gray-400 pointer-events-none">
                <i className="fas fa-caret-down" />
              </span>
            </Listbox.Button>

            <Transition
              show={open}
              as={Fragment}
              enter="transition ease-out duration-100"
              enterFrom="transform opacity-0 scale-95"
              enterTo="transform opacity-100 scale-100"
              leave="transition ease-in duration-75"
              leaveFrom="transform opacity-100 scale-100"
              leaveTo="transform opacity-0 scale-95"
            >
              {renderMenuItem}
            </Transition>
          </div>
        </>
      )}
    </Listbox>
  );
};
