import dynamic from "next/dynamic";

export const DatePicker = dynamic(() => import("./DatePicker"), {
  ssr: false,
});

export const DatePickerRange = dynamic(() => import("./DatePickerRange"), {
  ssr: false,
});
