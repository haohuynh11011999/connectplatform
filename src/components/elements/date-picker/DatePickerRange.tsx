// Ref calendarElement is null causes "Cannot read property 'removeEventListener' of null"
// https://github.com/Kiarash-Z/react-modern-calendar-datepicker/issues/204#issuecomment-757520714

import ReactDatePicker, {
  DayRange,
} from "@hassanmojab/react-modern-calendar-datepicker";
import classNames from "classnames";
import dayjs from "dayjs";
import localizedFormat from "dayjs/plugin/localizedFormat";
import React, { useEffect, useState } from "react";

import { dateToDayValue, dayValueToDate } from "./utils";

dayjs.extend(localizedFormat);

type IChangeEvent = {
  target: { value: { from: Date; to: Date } };
};

type IProps = {
  value?: {
    from?: Date;
    to?: Date;
  };
  placeholder?: string;
  isDisabled?: boolean;
  onChange?: (event: IChangeEvent) => void;
};

const DatePickerRange: React.FC<IProps> = ({
  value,
  onChange,
  isDisabled,
  placeholder,
}) => {
  const [selectedDayRange, setSelectedDayRange] = useState({
    from: value?.from ? dateToDayValue(value?.from) : null,
    to: value?.to ? dateToDayValue(value?.to) : null,
  });

  useEffect(() => {
    setSelectedDayRange({
      from: value?.from ? dateToDayValue(value?.from) : null,
      to: value?.to ? dateToDayValue(value?.to) : null,
    });
  }, [value?.from, value?.to]);

  const handleChange = (dayRange: DayRange) => {
    setSelectedDayRange(dayRange);

    if (onChange && dayRange.from && dayRange.to) {
      onChange({
        target: {
          value: {
            from: dayValueToDate(dayRange.from),
            to: dayValueToDate(dayRange.to),
          },
        },
      });
    }
  };

  const renderCustomInput = ({ ref }) => {
    const from = dayjs(
      selectedDayRange?.from ? dayValueToDate(selectedDayRange?.from) : null
    );
    const to = dayjs(
      selectedDayRange?.to ? dayValueToDate(selectedDayRange?.to) : null
    );
    const fromFormatValue = from.isValid() ? from.format("L") : "";
    const toFormatValue = to.isValid() ? to.format("L") : "";

    const value =
      fromFormatValue && toFormatValue
        ? `${fromFormatValue} - ${toFormatValue}`
        : "";

    return (
      <input
        ref={ref}
        className={classNames(
          "w-full h-10 px-4 bg-gray-100 outline-none cursor-pointer focus:ring-1 ring-primary-400 rounded-md",
          {
            "border cursor-not-allowed !bg-gray-50 text-gray-400": isDisabled,
          }
        )}
        readOnly
        placeholder={placeholder || "Select date"}
        value={value}
        disabled={isDisabled}
      />
    );
  };

  return (
    <ReactDatePicker
      locale="en"
      value={selectedDayRange}
      onChange={handleChange}
      renderInput={renderCustomInput}
      shouldHighlightWeekends
      colorPrimary="#f37f26"
      colorPrimaryLight="#ffd3b2"
      slideAnimationDuration="0.3s"
      wrapperClassName="TractionWork-DatePicker"
      calendarClassName="TractionWork-DatePicker-calendar"
      calendarPopperPosition="auto"
    />
  );
};

export default DatePickerRange;
