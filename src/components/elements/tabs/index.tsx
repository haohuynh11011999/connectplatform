import classNames from "classnames";
import React from "react";

export type ITab = {
  key: string;
  name: string;
};

type ITabProps = {
  tab: ITab;
  isActive?: boolean;
  onClick: (value: string) => void;
};

const Tab: React.FC<ITabProps> = ({ tab, isActive, onClick }) => {
  const handleClick = () => {
    if (onClick) {
      onClick(tab.key);
    }
  };

  return (
    <div
      className={classNames(
        "relative",
        "flex items-center font-medium px-4 p-1 my-1 rounded",
        "outline-none focus:outline-none",
        "transition-colors duration-300",
        "cursor-pointer whitespace-nowrap",
        {
          "hover:bg-gray-200": !isActive,
          "bg-white shadow z-10": isActive,
        }
      )}
      onClick={handleClick}
      role="menuitem"
      tabIndex={0}
      onKeyPress={handleClick}
    >
      <span className="select-none">{tab.name}</span>
    </div>
  );
};

type ITabsProps = {
  tabs: ITab[];
  activeKey: string;
  classnames?: string;
  onChange: (key: string) => void;
};

export const Tabs: React.FC<ITabsProps> = ({
  tabs,
  activeKey,
  classnames,
  onChange,
}) => {
  return (
    <div
      className={classNames(
        "inline-flex items-center h-10 overflow-x-auto bg-gray-100 shadow-inner divide-x px-6px divide-gray-200 rounded-md hide-scrollbar",
        classnames
      )}
    >
      {tabs.map((tab) => {
        return (
          <Tab
            key={tab.key}
            tab={tab}
            isActive={tab.key === activeKey}
            onClick={onChange}
          />
        );
      })}
    </div>
  );
};
