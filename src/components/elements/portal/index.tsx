import { ReactNode, useEffect, useRef, useState } from "react";
import { createPortal } from "react-dom";

type IProps = {
  children: ReactNode;
};

export const Portal: React.FC<IProps> = ({ children }) => {
  const [isRendered, setIsRendered] = useState(false);

  const appRoot = useRef<HTMLElement>(null);
  const el = useRef<HTMLDivElement>(null);

  useEffect(() => {
    el.current = document.createElement("div");
    appRoot.current = document.getElementById("__next");
    appRoot.current.appendChild(el.current);

    setIsRendered(true);

    return () => {
      appRoot.current.removeChild(el.current);
    };
  }, []);

  if (!isRendered) {
    return null;
  }

  return createPortal(children, el.current);
};
