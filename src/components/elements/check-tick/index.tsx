import classNames from "classnames";

type CheckTickPropsType = {
  isActive: boolean;
  isDisabled?: boolean;
  className?: string;
  onChange?: (e: React.MouseEvent | React.KeyboardEvent) => void;
};

export const CheckTick: React.FC<CheckTickPropsType> = ({
  isActive,
  isDisabled,
  className,
  onChange,
}) => {
  return (
    <div
      className={classNames(
        "flex items-center justify-center w-6 h-6 text-gray-400 bg-white border rounded-full cursor-pointer",
        {
          "border-success-500": isActive,
          "cursor-not-allowed": isDisabled,
        },
        className
      )}
      onClick={onChange}
      role="checkbox"
      aria-checked={isActive}
      tabIndex={0}
      onKeyPress={null}
    >
      {isActive ? (
        <i className="fas fa-check text-success-500 animate__animated animate__tada" />
      ) : null}
    </div>
  );
};
