/* eslint-disable prettier/prettier */

type IRadio = {
  value: string;
  name: string;
  defaultChecked?: boolean;
  checkValue?: string;
  onChange?: React.ChangeEventHandler<HTMLInputElement>;
};
export const Radio: React.FC<IRadio> = ({
  name,
  value,
  defaultChecked,
  checkValue,
  onChange,
}) => {
  return (
    <div className="block">
      <label className="inline-flex items-center cursor-pointer">
        <input
          type="radio"
          value={value}
          defaultChecked={defaultChecked}
          checked={value === checkValue}
          onChange={onChange}
          name="radio"
          // className="w-4 h-4 bg-white border rounded-full shadow appearance-none cursor-pointer border-primary-400 ring-white ring-2 ring-inset hover:shadow-sm focus:bg-primary-400"
          className="w-4 h-4"
        />
        <span className="ml-2">{name}</span>
      </label>
    </div>
  );
};

type IRadioGroup = {
  options: IRadio[];
  defaultValue?: string;
  value?: string;
  onChange: React.ChangeEventHandler<HTMLInputElement>;
};
export const RadioGroup: React.FC<IRadioGroup> = ({
  options,
  defaultValue,
  value,
  onChange,
}) => {
  return (
    <div className="flex items-center justify-between">
      {options.map((option, index) => (
        <Radio
          key={index}
          value={option.value}
          name={option.name}
          onChange={onChange}
          checkValue={value}
          defaultChecked={option.value === defaultValue}
        />
      ))}
    </div>
  );
};
