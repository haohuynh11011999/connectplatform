/* eslint-disable @next/next/no-img-element */
import { useRouter } from "next/router";

import { LockIcon } from "~/assets";

import { Button } from "../button";

export const NotPermission = () => {
  const router = useRouter();

  const _handleBack = () => router.back();
  return (
    <div className="flex flex-col items-center flex-1 w-full mx-auto md:w-3/4">
      <div className="pb-6 w-60">
        <LockIcon className="w-24 h-auto mx-auto" />
      </div>
      <h3 className="pb-2 text-2xl font-semibold text-center text-black">
        {`You don't have permission to view this page`}
      </h3>
      <span className="pb-4 text-base text-center text-gray-600">
        Contact admin for permission to view this page
      </span>
      <Button
        title="Go back"
        type="primary"
        onClick={_handleBack}
        icon={<i className="fas fa-arrow-left" />}
      />
    </div>
  );
};
