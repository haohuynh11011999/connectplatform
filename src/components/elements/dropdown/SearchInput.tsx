import React, { useEffect, useRef } from "react";

type IInputProps = {
  isFocus?: boolean;
  value?: string;
  onChange: React.ChangeEventHandler<HTMLInputElement>;
};

export const SearchInput: React.FC<IInputProps> = ({
  value,
  onChange,
  isFocus,
}) => {
  const ref = useRef<HTMLInputElement>(null);

  useEffect(() => {
    if (ref.current && isFocus) {
      setTimeout(() => {
        ref.current?.focus();
        ref.current?.select();
      }, 100);
    }
  }, [isFocus]);

  const handleKeyDown = (e: React.KeyboardEvent<HTMLInputElement>) => {
    e.stopPropagation();
  };

  return (
    <input
      ref={ref}
      className="w-full px-2 py-1 bg-white border outline-none focus:outline-none focus:border-primary-400 rounded-md"
      //
      value={value}
      onChange={onChange}
      onKeyDown={handleKeyDown}
      placeholder="Search ..."
    />
  );
};
