import { Menu, Transition } from "@headlessui/react";
import classNames from "classnames";
import React, { useState } from "react";
import { useMemo } from "react";
import { Fragment } from "react";

import { nonAccentVietnamese } from "~/utils/string";

import { SearchInput } from "./SearchInput";

export type MenuItem = {
  value: string;
  name: string;
  prefix?: JSX.Element;
  suffix?: JSX.Element;
};

type Props = {
  menus?: MenuItem[];
  onClick?: (key: string) => void;
  placement?: "left" | "right";
  overlay?: JSX.Element;
  animation?: boolean;
  maxHeight?: number | string;
  disabled?: boolean;
  widthContainer?: string;
  isSearchable?: boolean;
};

export const Dropdown: React.FC<Props> = ({
  children,
  menus = [],
  onClick,
  placement = "right",
  overlay,
  animation = true,
  maxHeight,
  disabled,
  widthContainer,
  isSearchable = false,
}) => {
  const [keyword, setKeyword] = useState("");

  const transition = animation
    ? {
        enter: "transition ease-out duration-100",
        enterFrom: "transform opacity-0 scale-95",
        enterTo: "transform opacity-100 scale-100",
        leave: "transition ease-in duration-75",
        leaveFrom: "transform opacity-100 scale-100",
        leaveTo: "transform opacity-0 scale-95",
      }
    : {};

  const menusFiltered = useMemo(() => {
    if (!isSearchable) {
      return menus;
    }

    return menus.filter((menu) => {
      if (!keyword) {
        return true;
      }

      const source = nonAccentVietnamese(menu.name.toLowerCase());
      const searchKey = nonAccentVietnamese(keyword.toLowerCase());

      return source.indexOf(searchKey) > -1;
    });
  }, [isSearchable, menus, keyword]);

  const handleSearchChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setKeyword(e.target.value);
  };

  return (
    <Menu as="div" className="relative inline-block text-left">
      {({ open }) => (
        <>
          <Menu.Button
            className="flex select-none focus:outline-none"
            disabled={disabled}
          >
            {children}
          </Menu.Button>

          <Transition show={open} as={Fragment} {...transition}>
            <Menu.Items
              static
              className={classNames(
                "absolute w-56 mt-2 z-50 bg-white shadow-lg origin-top-right rounded-md ring-1 ring-black ring-opacity-5 focus:outline-none overflow-auto",
                {
                  "left-0": placement === "left",
                  "right-0": placement !== "left",
                }
              )}
              style={{ maxHeight: maxHeight || 320, width: widthContainer }}
            >
              {overlay ? (
                <Menu.Item>{overlay}</Menu.Item>
              ) : (
                <div>
                  {isSearchable && (
                    <div className="sticky top-0 z-10 px-2 pt-2 bg-white">
                      <SearchInput
                        value={keyword}
                        onChange={handleSearchChange}
                        isFocus={open}
                      />
                    </div>
                  )}

                  <div className="py-2">
                    {menusFiltered.map((m) => {
                      return (
                        <Menu.Item
                          key={m.value}
                          onClick={() => onClick && onClick(m.value)}
                        >
                          {({ active }) => (
                            <div
                              className={classNames(
                                "flex items-center px-4 py-2 cursor-pointer transition-all duration-300 hover:text-primary-400 select-none leading-none",
                                {
                                  "bg-gray-100 text-gray-900": active,
                                  "text-gray-700": !active,
                                }
                              )}
                            >
                              {m.prefix}
                              {m.name}
                              {m.suffix}
                            </div>
                          )}
                        </Menu.Item>
                      );
                    })}

                    {menusFiltered.length === 0 && (
                      <div className="px-4 py-2 text-gray-400">
                        <a>No results found</a>
                      </div>
                    )}
                  </div>
                </div>
              )}
            </Menu.Items>
          </Transition>
        </>
      )}
    </Menu>
  );
};
