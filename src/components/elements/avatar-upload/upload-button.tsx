/* eslint-disable @next/next/no-img-element */
import "react-activity/dist/Spinner.css";

import { CameraIcon } from "@heroicons/react/outline";
import { asUploadButton } from "@rpldy/upload-button";
import { useBatchStartListener, useItemFinishListener } from "@rpldy/uploady";
import React, { useState } from "react";
import Spinner from "react-activity/dist/Spinner";

import { getColorFromText } from "../avatar/utils";

export const ButtonUpload = asUploadButton((props) => {
  const [loading, setLoading] = useState(false);
  useBatchStartListener(() => {
    setLoading(true);
  });

  useItemFinishListener(() => {
    setLoading(false);
  });

  return (
    <div {...props} className="relative w-32 h-32 overflow-hidden rounded-md">
      {props.url ? (
        <img
          alt="user-avatar"
          src={props.url || "/images/default-avatar.png"}
          className="object-cover w-32 h-32 cursor-pointer"
        />
      ) : (
        <div
          className="flex items-center justify-center w-full h-full"
          style={{ backgroundColor: getColorFromText(props.char) }}
        >
          <span className=" text-4xl text-white">{props.char}</span>
        </div>
      )}
      <div className="absolute top-0 z-10 flex items-center justify-center w-full h-full bg-black opacity-0 cursor-pointer hover:opacity-100 bg-opacity-20 transition-opacity duration-300">
        <CameraIcon width={40} height={40} color="#ffffff" />
      </div>

      {loading ? (
        <div className="absolute top-0 z-20 flex items-center justify-center w-full h-full bg-white opacity-100 bg-opacity-30">
          <Spinner color="white" />
        </div>
      ) : (
        <React.Fragment />
      )}
    </div>
  );
});
