import classNames from "classnames";
import Link from "next/link";

import i18n from "~/../i18n";

const tabs = [
  {
    name: i18n.t("setting:tab.team"),
    key: "team",
    href: "/settings/teams",
  },
  {
    name: i18n.t("setting:tab.user"),
    key: "user",
    href: "/settings/users",
  },
  {
    name: i18n.t("setting:tab.configuration"),
    key: "configuration",
    href: "/settings/teams",
  },
  {
    name: i18n.t("setting:tab.billing"),
    key: "billing",
    href: "/settings/teams",
  },
];

type IProps = {
  activeKey: "team" | "configuration" | "billing" | "user";
};

type ITabProps = {
  name: string;
  href: string;
  isActive: boolean;
};

const Tab: React.FC<ITabProps> = ({ name, href, isActive }) => (
  <Link href={href} passHref>
    <div
      className={classNames(
        "flex items-center px-6 align-middle rounded cursor-pointer text-sm font-semibold hover:bg-white",
        { "bg-white": isActive }
      )}
    >
      {name}
    </div>
  </Link>
);

export const Tabs: React.FC<IProps> = ({ activeKey }) => (
  <div className="flex flex-row h-10 p-1 bg-gray-100 rounded">
    {tabs.map((tab) => (
      <div
        key={tab.key}
        className="flex border-l border-gray-200 first:border-l-0"
      >
        <Tab {...tab} isActive={tab.key === activeKey} />
      </div>
    ))}
  </div>
);
