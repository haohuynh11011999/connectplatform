import { TFuncKey, TFunction } from "react-i18next";
import * as yup from "yup";

export function buildYupLocale(
  _: unknown,
  t: TFunction<("message" | "label")[]>
): void {
  yup.setLocale({
    mixed: {
      required: (params: { label: TFuncKey<"label"[]> }) => {
        return t(params.label) + t("message:error.required");
      },
    },
    string: {
      email: t("message:error.formatEmail"),
    },
  });
}
