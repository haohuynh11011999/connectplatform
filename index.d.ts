declare interface ENVIRONMENT {
  WEB_GATEWAY_API: string;
  BASEURL_API_AUTH: string;
  BASEURL_API_COMPANY: string;
  NODE_ENV: string;
  DEVELOPMENT_SERVICES: string;
  BASEURL_API_DRIVE: string;
  BASEURL_API_EOS_CORE: string;
  TENANT_ID: string;
  INTERNAL_API_AUTH: string;
  INTERNAL_API_COMPANY: string;
  INTERNAL_API_DRIVE: string;
  INTERNAL_API_EOS_CORE: string;
}

declare interface Window {
  __ENV__: ENVIRONMENT;
}

declare type AnyObject = {
  //eslint-disable-next-line
  [key: string]: any;
};

declare module "*.svg" {
  const content: React.ComponentType<
    React.SVGProps<SVGSVGElement> | CustomIconComponentProps
  >;
  export default content;
}
